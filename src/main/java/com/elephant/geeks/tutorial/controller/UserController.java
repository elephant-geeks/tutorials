package com.elephant.geeks.tutorial.controller;

import com.elephant.geeks.tutorial.controller.value.UserVO;
import com.elephant.geeks.tutorial.domain.User;
import com.elephant.geeks.tutorial.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/um/api/v1/")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("user")
    public ResponseEntity createUser(@RequestBody UserVO userVO) {
        User user = User.build(userVO);
        userService.save(user);
        return new ResponseEntity(user, HttpStatus.CREATED);
    }
}
