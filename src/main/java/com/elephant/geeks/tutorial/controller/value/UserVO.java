package com.elephant.geeks.tutorial.controller.value;
import lombok.Setter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.ToString;


@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserVO {
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
