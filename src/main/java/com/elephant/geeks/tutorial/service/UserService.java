package com.elephant.geeks.tutorial.service;

import com.elephant.geeks.tutorial.domain.User;
import com.elephant.geeks.tutorial.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User save(User user) {
        return userRepository.save(user);
    }
}
