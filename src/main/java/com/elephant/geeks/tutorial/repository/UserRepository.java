package com.elephant.geeks.tutorial.repository;

import com.elephant.geeks.tutorial.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,String> {

}

